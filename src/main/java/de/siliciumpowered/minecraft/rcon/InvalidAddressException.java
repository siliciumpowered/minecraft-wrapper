package de.siliciumpowered.minecraft.rcon;

import java.net.InetSocketAddress;

public class InvalidAddressException extends MinecraftRconException {
    private final InetSocketAddress address;

    InvalidAddressException(final InetSocketAddress address, final Throwable cause) {
        super("Invalid address: " + address, cause);
        this.address = address;
    }

    public InetSocketAddress getAddress() {
        return address;
    }
}