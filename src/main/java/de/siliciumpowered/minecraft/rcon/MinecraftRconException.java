package de.siliciumpowered.minecraft.rcon;

public class MinecraftRconException extends RuntimeException {
    public MinecraftRconException(final String message, final Throwable cause) {
        super(message, cause);
    }
}