package de.siliciumpowered.minecraft.rcon;

import com.ibasco.agql.protocols.valve.source.query.SourceRconAuthStatus;
import com.ibasco.agql.protocols.valve.source.query.client.SourceRconClient;
import com.ibasco.agql.protocols.valve.source.query.exceptions.RconNotYetAuthException;
import com.ibasco.agql.protocols.valve.source.query.exceptions.SourceRconAuthException;

import javax.annotation.Nonnull;
import java.net.InetSocketAddress;

public class MinecraftRconAGQLClient extends BaseMinecraftRconClient {
    @Nonnull private final InetSocketAddress address;
    @Nonnull private final SourceRconClient underlyingClient;
    @Nonnull private final MinecraftRconResponseParser parser;

    /**
     * Instantiates a connected and authorized RCON client.
     * Instantiation is canceled for any kind of error and connection will be closed.
     *
     * @param address          Not the Minecraft server port but the RCON port
     * @param underlyingClient {@link SourceRconClient} with sending terminating packet disabled
     * @throws InvalidAddressException      when connection could not be established
     * @throws AuthorisationFailedException including the reason when authorization failed
     * @throws MinecraftRconException       when an unknown error occurred
     */
    MinecraftRconAGQLClient(@Nonnull final InetSocketAddress address,
                            @Nonnull final String password,
                            @Nonnull final SourceRconClient underlyingClient,
                            @Nonnull final MinecraftRconResponseParser parser) {
        this.address = address;
        this.underlyingClient = underlyingClient;
        this.parser = parser;
        SourceRconAuthStatus authStatus;
        try {
            authStatus = underlyingClient.authenticate(address, password).join();
        } catch (Throwable error) {
            close();
            if (error.getSuppressed().length > 0 && error.getSuppressed()[0] instanceof SourceRconAuthException) {
                throw new InvalidAddressException(address, error);
            } else {
                throw new MinecraftRconException("Unknown error occurred", error);
            }
        }
        if (authStatus == null || !authStatus.isAuthenticated()) {
            close();
            final String reason = authStatus == null || authStatus.getReason() == null || authStatus.getReason().trim().length() == 0 ? "No reason given (check your password)" : authStatus.getReason();
            throw new AuthorisationFailedException(reason);
        }
    }

    @Override
    @Nonnull
    protected MinecraftRconResponseParser getParser() {
        return parser;
    }

    @Nonnull
    @Override
    protected String execute(final String command) {
        try {
            return underlyingClient.execute(address, command).join();
        } catch (RconNotYetAuthException error) {
            throw new MinecraftRconException("Authorization gone", error);
        } catch (Throwable error) {
            throw new MinecraftRconException("Command execution failed", error);
        }
    }

    @Override
    public void close() {
        try {
            underlyingClient.close();
        } catch (Throwable error) {
            throw new MinecraftRconException("Closing RCON connection failed", error);
        }
    }
}