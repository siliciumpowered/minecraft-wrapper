package de.siliciumpowered.minecraft.rcon;

import com.ibasco.agql.protocols.valve.source.query.client.SourceRconClient;

import java.net.InetSocketAddress;
import java.util.Collection;

public interface MinecraftRconClient extends AutoCloseable {
    static MinecraftRconClient connect(final InetSocketAddress address, final String password) {
        return new MinecraftRconAGQLClient(address,
                                           password,
                                           new SourceRconClient(false),
                                           new MinecraftRconResponseParser());
    }

    Collection<String> getPlayers();

    void sayAsRcon(String message);

    String getSeed();

    void stopServer();

    @Override
    void close() throws MinecraftRconException;
}