package de.siliciumpowered.minecraft.rcon;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

import static java.util.stream.Collectors.toCollection;

class MinecraftRconResponseParser {

    @Nonnull
    Collection<String> parsePlayers(@Nonnull String rconResponse) {
        final String suffix = rconResponse.substring(rconResponse.indexOf(':') + 1);
        final Collection<String> entries = Arrays.asList(suffix.split(", "));
        return entries.stream()
                      .map(String::trim)
                      .filter(entry -> !entry.isEmpty())
                      .collect(toCollection(LinkedList::new));
    }

    @Nonnull
    String parseSeed(@Nonnull final String rconResponse) {
        return rconResponse.substring(6);
    }
}