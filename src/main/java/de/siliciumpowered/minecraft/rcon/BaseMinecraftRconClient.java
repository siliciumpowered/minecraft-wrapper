package de.siliciumpowered.minecraft.rcon;

import javax.annotation.Nonnull;
import java.util.Collection;

public abstract class BaseMinecraftRconClient implements MinecraftRconClient {
    @Override
    @Nonnull
    public Collection<String> getPlayers() {
        return getParser().parsePlayers(execute("list"));
    }

    @Override
    public void sayAsRcon(final String message) {
        execute("say " + message);
    }

    @Override
    @Nonnull
    public String getSeed() {
        return getParser().parseSeed(execute("seed"));
    }

    @Override
    public void stopServer() {
        execute("stop");
    }

    @Nonnull
    protected abstract MinecraftRconResponseParser getParser();

    @Nonnull
    protected abstract String execute(String command);
}