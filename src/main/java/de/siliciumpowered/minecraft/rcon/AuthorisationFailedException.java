package de.siliciumpowered.minecraft.rcon;

public class AuthorisationFailedException extends MinecraftRconException {
    private final String reason;

    AuthorisationFailedException(final String reason) {
        super("Unable to authorize: " + reason, null);
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }
}