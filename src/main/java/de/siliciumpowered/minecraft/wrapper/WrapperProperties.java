package de.siliciumpowered.minecraft.wrapper;

import javax.annotation.Nullable;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Properties;
import java.util.StringJoiner;

import static java.util.Optional.ofNullable;

class WrapperProperties {
    private static final String DEFAULT_RCON_HOSTNAME = "localhost";
    private static final String DEFAULT_RCON_PORT = "25575";
    private static final String DEFAULT_RCON_PASSWORD = "password";
    private static final String DEFAULT_MINECRAFT_COMMAND = "./ServerStart.sh";

    final InetSocketAddress address;
    final String password;
    final String minecraftCommand;

    WrapperProperties(@Nullable final String configurationFilePath) {
        Properties properties = loadProperties(configurationFilePath);
        try {
            String hostname = properties.getProperty("rcon.hostname", DEFAULT_RCON_HOSTNAME);
            int port = Integer.parseInt(properties.getProperty("rcon.port", DEFAULT_RCON_PORT));
            address = new InetSocketAddress(hostname, port);
        } catch (Throwable error) {
            throw new RuntimeException("Invalid hostname or port", error);
        }
        password = properties.getProperty("rcon.password", DEFAULT_RCON_PASSWORD);
        minecraftCommand = properties.getProperty("minecraft.command", DEFAULT_MINECRAFT_COMMAND);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", WrapperProperties.class.getSimpleName() + "[", "]")
                .add("address=" + address)
                .add("password='[hidden]'")
                .add("minecraftCommand='" + minecraftCommand + "'")
                .toString();
    }

    private static Properties loadProperties(@Nullable final String maybeConfigurationFilePath) {
        return ofNullable(maybeConfigurationFilePath).map(configurationFilePath -> {
            try (FileInputStream in = new FileInputStream(configurationFilePath)) {
                Properties properties = new Properties();
                properties.load(in);
                return properties;
            } catch (IOException error) {
                throw new RuntimeException("Could not load properties file: " + configurationFilePath, error);
            }
        }).orElseGet(Properties::new);
    }
}