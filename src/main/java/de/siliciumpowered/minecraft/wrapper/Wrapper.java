package de.siliciumpowered.minecraft.wrapper;

import de.siliciumpowered.minecraft.rcon.AuthorisationFailedException;
import de.siliciumpowered.minecraft.rcon.InvalidAddressException;
import de.siliciumpowered.minecraft.rcon.MinecraftRconClient;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.logging.Logger;

import static de.siliciumpowered.minecraft.wrapper.Util.getFirstOrNull;

public class Wrapper {
    private static final Logger LOGGER = Logger.getLogger(Wrapper.class.getName());

    public static void main(final String[] commandLineParameters) {
        WrapperProperties properties;
        try {
            properties = new WrapperProperties(getFirstOrNull(commandLineParameters));
            LOGGER.info("Loaded properties: " + properties);
        } catch (Throwable error) {
            LOGGER.severe(error.getMessage());
            System.exit(1);
            return;
        }

        new Wrapper(properties);
    }

    private final WrapperProperties properties;
    private final Thread shutdownHook;

    private Wrapper(@Nonnull final WrapperProperties properties) {
        this.properties = properties;
        shutdownHook = createShutdownHook();
        registerShutdownHook();
        startAndWaitForStopOfServer();
    }

    private void startAndWaitForStopOfServer() {
        try {
            LOGGER.info("Executing Minecraft Server command");
            Process minecraftProcess = new ProcessBuilder(properties.minecraftCommand.split(" ")).inheritIO()
                                                                                                 .start();
            minecraftProcess.waitFor();
            minecraftProcess.waitFor();
        } catch (IOException | InterruptedException error) {
            unregisterShutdownHook();
            if (error.getMessage().endsWith("No such file or directory")) {
                LOGGER.severe("Could not find executable of Minecraft command: " + properties.minecraftCommand);
            } else {
                throw new RuntimeException(error);
            }
        }
    }

    private void stopServerViaRcon() {
        LOGGER.info("Executing shutdown via RCON");
        try (MinecraftRconClient client = MinecraftRconClient.connect(properties.address, properties.password)) {
            client.stopServer();
        } catch (InvalidAddressException error) {
            LOGGER.severe("Unable to connect to " + error.getAddress());
        } catch (AuthorisationFailedException error) {
            LOGGER.severe("Authorization failed: " + error.getReason());
        }
    }

    private Thread createShutdownHook() {
        final Thread mainThread = Thread.currentThread();
        return new Thread(() -> {
            stopServerViaRcon();
            try {
                LOGGER.info("Waiting for Server to stop");
                mainThread.join();
            } catch (InterruptedException error) {
                throw new RuntimeException(error);
            }
        }, "shutdown-hook");
    }

    private void registerShutdownHook() {
        LOGGER.info("Registering shutdown hook");
        Runtime.getRuntime().addShutdownHook(shutdownHook);
    }

    private void unregisterShutdownHook() {
        if (shutdownHook != null) {
            LOGGER.info("Unregistering shutdown hook");
            Runtime.getRuntime().removeShutdownHook(shutdownHook);
        }
    }
}