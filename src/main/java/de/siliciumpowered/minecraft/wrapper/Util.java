package de.siliciumpowered.minecraft.wrapper;

class Util {
    static <T> T getFirstOrNull(final T[] array) {
        return array.length > 0 ? array[0] : null;
    }
}