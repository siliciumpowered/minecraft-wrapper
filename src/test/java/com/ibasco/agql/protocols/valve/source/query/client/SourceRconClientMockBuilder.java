package com.ibasco.agql.protocols.valve.source.query.client;

import com.ibasco.agql.protocols.valve.source.query.SourceRconAuthStatus;
import com.ibasco.agql.protocols.valve.source.query.exceptions.RconNotYetAuthException;
import com.ibasco.agql.protocols.valve.source.query.exceptions.SourceRconAuthException;

import java.net.InetSocketAddress;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class SourceRconClientMockBuilder {
    private final SourceRconClient client = mock(SourceRconClient.class);

    public SourceRconClient build() {
        return client;
    }

    public SourceRconClientMockBuilder withResponseFor(final String command,
                                                       final String response) {
        try {
            given(client.execute(any(InetSocketAddress.class), eq(command))).willReturn(completedFuture(response));
        } catch (RconNotYetAuthException error) {
            throw new RuntimeException(error);
        }
        return this;
    }

    public SourceRconClientMockBuilder withAuthenticationSuccessfulFor(final String password) {
        given(client.authenticate(any(InetSocketAddress.class), eq(password)))
                .willReturn(completedFuture(new SourceRconAuthStatus(true, "")));
        return this;
    }

    public SourceRconClientMockBuilder withReasonForUnsuccessfulAuthentication(final String reason) {
        given(client.authenticate(any(InetSocketAddress.class), anyString()))
                .willReturn(completedFuture(new SourceRconAuthStatus(false, reason)));
        return this;
    }

    public SourceRconClientMockBuilder withInvalidAddress() {
        Throwable throwable = new RuntimeException();
        throwable.addSuppressed(new SourceRconAuthException());
        given(client.authenticate(any(InetSocketAddress.class), anyString()))
                .willThrow(throwable);
        return this;
    }
}
