package de.siliciumpowered.minecraft.wrapper;

import org.junit.jupiter.api.Test;

import static de.siliciumpowered.minecraft.wrapper.Util.getFirstOrNull;
import static org.assertj.core.api.Assertions.assertThat;

class UtilTest {

    @Test
    void shouldGetFirst() {
        // given
        String[] array = new String[]{"<first>", "<second>"};

        // when
        String result = getFirstOrNull(array);

        // then
        assertThat(result).isEqualTo("<first>");
    }

    @Test
    void shouldFallbackWithNull() {
        // given
        String[] array = new String[]{};

        // when
        String result = getFirstOrNull(array);

        // then
        assertThat(result).isNull();
    }
}