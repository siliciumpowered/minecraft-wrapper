package de.siliciumpowered.minecraft.rcon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

class MinecraftRconResponseParserTest {
    private MinecraftRconResponseParser parser;

    @BeforeEach
    void setUp() {
        parser = new MinecraftRconResponseParser();
    }

    @Test
    void shouldParseNoPlayers() {
        // given
        final String rconResponse = "There are 0/20 players online:";

        // when
        final Collection<String> result = parser.parsePlayers(rconResponse);

        // then
        assertThat(result).isEmpty();
    }

    @Test
    void shouldParseOnePlayers() {
        // given
        final String rconResponse = "There are 1/20 players online:player";

        // when
        final Collection<String> result = parser.parsePlayers(rconResponse);

        // then
        assertThat(result).containsExactly("player");
    }

    @Test
    void shouldParseMultiplePlayers() {
        // given
        final String rconResponse = "There are 3/20 players online:player1, player2, player3";

        // when
        final Collection<String> result = parser.parsePlayers(rconResponse);

        // then
        assertThat(result).containsExactly("player1", "player2", "player3");
    }

    @Test
    void shouldParseSeed() {
        // given
        final String rconResponse = "Seed: <seed>";

        // when
        final String result = parser.parseSeed(rconResponse);

        // then
        assertThat(result).isEqualTo("<seed>");
    }
}