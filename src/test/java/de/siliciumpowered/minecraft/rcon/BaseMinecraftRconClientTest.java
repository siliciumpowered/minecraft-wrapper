package de.siliciumpowered.minecraft.rcon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.annotation.Nonnull;
import java.util.Collection;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BaseMinecraftRconClientTest {
    private BaseMinecraftRconClient client;

    @Mock private MinecraftRconResponseParser parser;

    @BeforeEach
    void setUp() {
        client = spy(new BaseMinecraftRconClient() {
            @Nonnull
            @Override
            public MinecraftRconResponseParser getParser() {
                return parser;
            }

            @Nonnull
            @Override
            public String execute(String command) {
                return String.format("<rconResponse-for-%s>", command);
            }

            @Override
            public void close() throws MinecraftRconException {
            }
        });
    }

    @Test
    void shouldGetPlayers() {
        // given
        final Collection<String> players = emptyList();
        given(parser.parsePlayers("<rconResponse-for-list>")).willReturn(players);

        // when
        final Collection<String> result = client.getPlayers();

        // then
        assertThat(result).isSameAs(players);
    }

    @Test
    void shouldSayAsRcon() {
        // when
        client.sayAsRcon("<message>");

        // then
        verify(client).execute("say <message>");
    }

    @Test
    void souldGetSeed() {
        // given
        given(parser.parseSeed("<rconResponse-for-seed>")).willReturn("<seed>");

        // when
        final String result = client.getSeed();

        // then
        assertThat(result).isEqualTo("<seed>");
    }

    @Test
    void shouldStopServer() {
        // when
        client.stopServer();

        // then
        verify(client).execute("stop");
    }
}