package de.siliciumpowered.minecraft.rcon;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AuthorisationFailedExceptionTest {
    @Test
    void shouldHaveReason() {
        // given
        AuthorisationFailedException exception = new AuthorisationFailedException("<reason>");

        // when
        String result = exception.getReason();

        // then
        assertThat(result).isEqualTo("<reason>");
    }
}