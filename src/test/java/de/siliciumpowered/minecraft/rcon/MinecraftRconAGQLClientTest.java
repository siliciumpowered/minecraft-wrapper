package de.siliciumpowered.minecraft.rcon;

import com.ibasco.agql.protocols.valve.source.query.client.SourceRconClient;
import com.ibasco.agql.protocols.valve.source.query.client.SourceRconClientMockBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.InetSocketAddress;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@ExtendWith(MockitoExtension.class)
class MinecraftRconAGQLClientTest {
    @Mock private InetSocketAddress address;
    @Mock private MinecraftRconResponseParser parser;

    @Test
    void shouldExecuteCommand() {
        SourceRconClient underlyingClient = new SourceRconClientMockBuilder().withAuthenticationSuccessfulFor("<password>")
                                                                             .withResponseFor("<command>", "<response>")
                                                                             .build();

        MinecraftRconAGQLClient client = new MinecraftRconAGQLClient(address,
                                                                     "<password>",
                                                                     underlyingClient,
                                                                     parser);
        // when
        String result = client.execute("<command>");

        // then
        assertThat(result).isEqualTo("<response>");
    }

    @Test
    void shouldRecognizeInvalidAddress() {
        // given
        SourceRconClient underlyingClient = new SourceRconClientMockBuilder().withInvalidAddress()
                                                                             .build();

        // when
        Throwable thrown = catchThrowable(() -> new MinecraftRconAGQLClient(address,
                                                                            "<password>",
                                                                            underlyingClient,
                                                                            parser));

        // then
        assertThat(thrown).isInstanceOf(InvalidAddressException.class);
    }

    @Test
    void shouldHaveReasonForUnsuccessfulAuthentication() {
        // given
        SourceRconClient underlyingClient = new SourceRconClientMockBuilder().withReasonForUnsuccessfulAuthentication("<reason>")
                                                                             .build();

        // when
        Throwable thrown = catchThrowable(() -> new MinecraftRconAGQLClient(address,
                                                                            "<password>",
                                                                            underlyingClient,
                                                                            parser));

        // then
        assertThat(thrown).isInstanceOf(AuthorisationFailedException.class)
                          .hasMessage("Unable to authorize: <reason>");
    }

    @Test
    void shouldHavePasswordHintAsFallbackForEmptyReasonForUnsuccessfulAuthentication() {
        // given
        SourceRconClient underlyingClient = new SourceRconClientMockBuilder().withReasonForUnsuccessfulAuthentication("")
                                                                             .build();

        // when
        Throwable thrown = catchThrowable(() -> new MinecraftRconAGQLClient(address,
                                                                            "<password>",
                                                                            underlyingClient,
                                                                            parser));

        // then
        assertThat(thrown).isInstanceOf(AuthorisationFailedException.class)
                          .hasMessage("Unable to authorize: No reason given (check your password)");
    }
}