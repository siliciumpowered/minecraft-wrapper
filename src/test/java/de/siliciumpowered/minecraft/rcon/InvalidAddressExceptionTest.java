package de.siliciumpowered.minecraft.rcon;

import org.junit.jupiter.api.Test;

import java.net.InetSocketAddress;

import static org.assertj.core.api.Assertions.assertThat;

class InvalidAddressExceptionTest {
    @Test
    void shouldHaveAddress() {
        // given
        InetSocketAddress address = new InetSocketAddress("<hostname>", 12345);
        InvalidAddressException exception = new InvalidAddressException(address, null);

        // when
        InetSocketAddress result = exception.getAddress();

        // then
        assertThat(result).isSameAs(address);
    }
}